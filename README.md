# openssh-client

Container image with openssh-client and sshpass installed.

## Example usage
```sh
# See sshpass help
docker run -it --rm \
    --entrypoint sshpass \
    registry.gitlab.com/machinemode/openssh-client:latest

# SSH Tunnel through some bastion
docker run -it --rm -p "5434:5434" \
    -v pass:/pass:ro \
    -v /home/me/.ssh/known_hosts:/etc/ssh/ssh_known_hosts:ro \
    --entrypoint sshpass \
    registry.gitlab.com/machinemode/openssh-client:latest \
    -fpass ssh -g -L "*:5434:10.0.10.123:5432" -nNT me@my-bastion
```
