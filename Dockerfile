# syntax=docker/dockerfile:1
FROM alpine:latest

RUN apk update \
    && apk add openssh-client sshpass

ENTRYPOINT sh
